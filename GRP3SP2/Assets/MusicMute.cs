﻿using UnityEngine;
using System.Collections;

public class Audio : MonoBehaviour
{

    public AudioSource Music;
    void Start()
    {
        Music = GetComponent<AudioSource>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
            if (Music.mute)
                Music.mute = false;
            else
                Music.mute = true;
    }
}
