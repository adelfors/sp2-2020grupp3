﻿using UnityEngine;

using System.Collections;

public class Igelkott : MonoBehaviour

{

    public int moveSpeed = 140;  //per second 
    Vector3 computerDirection = Vector3.left;
    Vector3 moveDirection = Vector3.zero;
    Vector3 newPosition = Vector3.zero;
    void Start()
    {

    }
    void Update()
    {
        Vector3 newPosition = computerDirection * (moveSpeed * Time.deltaTime);
        newPosition = transform.position + newPosition;
        newPosition.x = Mathf.Clamp(newPosition.x, 46.3f, 69);
        transform.position = newPosition;
        if (newPosition.x >= 69)
        {
            newPosition.x = 69;
            computerDirection.x *= -1;
        }
        else if (newPosition.x <= 46.3f)
        {
            newPosition.x = 46.3f;
            computerDirection.x *= -1;
        }

        

    }

}