﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPaused = false;

    public GameObject pauseMenuUI;

    public GameObject boostBar;
    private void Start()
    {
        //pauseMenuUI = GameObject.Find("PauseCanvas").GetComponent<PauseMenu>();
        
    }
    void Update()
    {
        float moveVertical = Input.GetAxisRaw("Vertical");
        if (Input.GetButtonDown("Pause"))
        {
            Debug.Log("Pause");
            if (gameIsPaused == true)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1;
        gameIsPaused = false;
    }
    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        gameIsPaused = true;
    }

    public void mainMenu()
    {
        gameIsPaused = false;
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu"); //loads the main menu
    }
    public void quitGame()
    {
        Application.Quit(); //quit the game
    }
    public void restart()
    {
        gameIsPaused = false;
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
