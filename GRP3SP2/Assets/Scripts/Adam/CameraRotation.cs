﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    public float rotationSpeed;
    public Transform pivot;
    public GameObject player;

    public float moveRightStickRight;
    public float moveRightStickLeft;

    public float moveRightStickX;

    private void Start()
    {
   
    }
    void Update()
    {
        //Debug.Log("Movestick" + moveRightStickX);
        moveRightStickX = Input.GetAxisRaw("Mouse X");

       /* if (Mathf.Abs(moveRightStickX) > 0.01f && PauseMenu.gameIsPaused == false)
        {
            transform.parent.Rotate(Vector3.up * moveRightStickX * rotationSpeed);
        } */
        if (moveRightStickX > 0.02f && PauseMenu.gameIsPaused == false)
        {
            transform.parent.Rotate(Vector3.up * moveRightStickX * rotationSpeed);
        }
        if (moveRightStickX < -0.02f && PauseMenu.gameIsPaused == false)
        {
            transform.parent.Rotate(Vector3.down * -moveRightStickX * rotationSpeed);
        }
        if (moveRightStickX == 0)
        {
            transform.parent.rotation = transform.parent.rotation;
        }


        /* moveRightStickRight = Input.GetAxisRaw("Mouse X"); 
         moveRightStickLeft = -Input.GetAxisRaw("Mouse X");
         if (moveRightStickRight >= 0.09f) //if the stick is moved right
         {
             transform.parent.Rotate(Vector3.up * moveRightStickRight * rotationSpeed); //the camera is moved right
         }
         else
         {
             transform.parent.rotation = transform.parent.rotation; //otherwise the camera stays still
         }
         if (moveRightStickLeft >= 0.09f) //if the stick is moved left
         {
             transform.parent.Rotate(-Vector3.up * moveRightStickLeft * rotationSpeed); //the camera is moved left
         }
         else
         {
             transform.parent.rotation = transform.parent.rotation; //otherwise the camera stays still
         } */
        // old rotation code: transform.parent.rotate(Vector3.up, Input.GetAxis("Mouse X") * rotationSpeed);
    }
}
