﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoostBar : MonoBehaviour
{
    public Image currentBoostBar;
    public Text ratioText;

    private float boostpoint = 100;
    private float maxBoostPoint = 150;

    private void Start()
    {

    }
    private void updateBoostBar()
    {
        float ratio = boostpoint / maxBoostPoint;
        currentBoostBar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        ratioText.text = (ratio * 100).ToString() + '%';
    }
}
