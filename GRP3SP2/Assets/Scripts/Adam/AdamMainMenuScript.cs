﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class AdamMainMenuScript : MonoBehaviour
{
    public GameObject OptionsMenu, GameMenu, StageSelectMenu;
    public GameObject firstOptionsButton, firstPlayButton, firstStageSelectButton, OptionsClosedFirstButton, PlayMenuClosedFirstButton, StageSelectClosedFirstButton;
    public void StartPontusLevel()
    {
        SceneManager.LoadScene("Pontus");
    }
    public void StartAndresLevel()
    {
        SceneManager.LoadScene("Andres 1");
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void OpenCredits()
    {
        SceneManager.LoadScene("Credits");
    }
    public void OpenOptions()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstOptionsButton);
    }
    public void CloseOptions()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(OptionsClosedFirstButton);
    }

    public void PressPlay()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstPlayButton);
    }
    public void ClosePlay()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(PlayMenuClosedFirstButton);
    }
    public void OpenStageSelect()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstStageSelectButton);
    }
    public void CloseStageSelect()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(StageSelectClosedFirstButton);
    }
}
