﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    private float startPos; //the lifts lowest point
    public float endPos; //the lifts highest point
    private float moveUp; //how long it takes from the moment the lift stands still to when it starts moving up again
    public float moveUpTimer; //how long it takes from the moment the lift stands still to when it starts moving up again  
    private float moveDown; //how long it takes from the moment the lift stands still to when it starts moving down again
    public float moveDownTimer; //how long it takes from the moment the lift stands still to when it starts moving down again
    private int elevatorState; //the state which shows if the elevator is 1.at the bottom. 2.moving up. 3.at the top. 4.moving down.
    public float speed;

    public bool blocked;
    public LayerMask blockingLayer;

    public AudioClip liftSound;
    public AudioSource source;

    public bool canPlaySound;
    // Start is called before the first frame update
    void Start()
    {
        elevatorState = 1;
        startPos = transform.position.y;
        moveUp = moveUpTimer;
        moveDown = moveDownTimer;
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.BoxCast(transform.position + Vector3.down, new Vector3(2, 0.2f, 2), Vector3.down, transform.rotation, 1, blockingLayer))
        {
            blocked = true;
            return;
        }
        else
        {
            blocked = false;
        }
        if (elevatorState == 1) //the lift is at the bottom
        {
            moveUpTimer -= Time.deltaTime; // the timer to move the lift up starts counting down
            if(moveUpTimer <= 0) //if the timer to move the lift up is 0
            {
                elevatorState = 2; //the state goes to 2
                if (canPlaySound == true)
                {
                    source.PlayOneShot(liftSound);
                }
            }
        }
        if (elevatorState == 2) 
        {
            moveUpTimer = moveUp;
            transform.Translate(Vector3.up * speed * Time.deltaTime); // move the lift up
            //SoundManager.PlaySound(SoundManager.Sound.g3_Elevator);
            if(transform.position.y >= endPos) //if the lifts transform position.y is equal to or higher than its highest position
            {
                elevatorState = 3; // the state goes to 3
            }
        }
        if (elevatorState == 3)
        {
            moveDownTimer -= Time.deltaTime; //the timer to move the lift down starts counting down
            if (moveDownTimer <= 0) //if the timer to move the lift down is 0
            {
                elevatorState = 4; //the state goes to 4
                if (canPlaySound == true)
                {
                    source.PlayOneShot(liftSound);
                }
            }
        }
        if (elevatorState == 4)
        {
            moveDownTimer = moveDown;
            transform.Translate(Vector3.down * speed * Time.deltaTime); //move the lift down
            if (transform.position.y <= startPos)
            {
                elevatorState = 1; // the states goes to 1
            }
        }
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            canPlaySound = true;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            canPlaySound = false;
        }
    }
}
