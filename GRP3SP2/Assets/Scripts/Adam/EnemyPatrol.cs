﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public float speed;
    public Transform moveSpot; // where the enemy can move to
    public bool going;
    public Transform moveSpot2; // where the enemy can move to
    private float waitTime; //time it takes from when enemy reaches a patrol point to head towards the next
    public float startWaitTime;
    void Start()
    {
        waitTime = startWaitTime;
        going = true;
    }
    void Update()
    {
        if (going == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, moveSpot2.position, speed * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, moveSpot.position, speed * Time.deltaTime);
        }
        if (Vector3.Distance(transform.position, moveSpot2.position) < 0.2f && going == true)
        {
            if (waitTime <= 0) //if the wait time is 0 or less
            {
                Debug.Log("GO!");
                waitTime = startWaitTime;
                going = false;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }
  
        if (Vector3.Distance(transform.position, moveSpot.position) < 0.2f && going == false)
        {
            if (waitTime <= 0) //if the wait time is 0 or less
            {
                Debug.Log("GO!");
                waitTime = startWaitTime;
                going = true;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }
    }
}
