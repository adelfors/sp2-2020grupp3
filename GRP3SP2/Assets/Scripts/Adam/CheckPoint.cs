﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    private GameMaster gm;
    public void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>();
    }
    public void OnTriggerEnter(Collider other)
    {
    if (other.gameObject.tag == "Player")
        {
            gm.lastCheckPointPos = transform.position;
            Destroy(gameObject);
        }
    }
}
