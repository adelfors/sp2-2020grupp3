﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{
    public float normalAccel; //the normal acceleration for the player
    public float slowAccel; //the slower acceleration if the acceleration limit has been reached
    public float accelLimit; //how fast the players velocity can reach before their acceleration becomes slower
    public float jumpForce; //how high the player can jump by tapping the button
    public float highJumpForce; //how high the player can jump by holding the button

    private float jumpTimeCounter; //what determines how long the player will stay in the air by holding the jump button
    public float jumpTime; //how long the player stays in the air by tapping the jump button
    private bool isJumping; //if the player is jumping
    private bool hasJumped; //if the player has recently jumped
    public float gravity;

    //boost
    public float boostMeter; //the boost meter
    private float maxBoostMeter;
    public bool isBoosting; //if the player is boosting
    public float boostAccel; //the acceleration if the player is boosting

    public Image currentBoostBar;
    public Text ratioText;
    public Text CooldownText;

    public bool hasBoosted;

    public float boostCooldown;
    private float boostCooldownTimer;

    //bounce
    public float bounce;

    //enemy push
    public float push;

    //brake 
    public bool isBraking;
    public float fullStop; //how long it takes for the player to come to a complete stop when braking
    private float fullStopTimer;

    public Ray downRay; //the players ray that points down at the ground
    public RaycastHit downHit; //the hitinfo for downRay that determines if its hit

    public bool isGrounded; //check if the character is grounded

    private Rigidbody rb; //the players rigidbody

    private Transform cameraDirection; //the direction the camera is facing

    public GameObject camObject; //the empty gameobject which will determine the players movement based on camera direction

    //pickup count
    private int count; //shows how many pickups the player has acquired
    public int numberOfPickups; //how many pickups are in the stage
    public Text countText;
    public int score; //how many points the player has acquired through pickups
    public Text scoreText; //the score displayed in text

    //sound
    public AudioClip boostSound;
    public AudioClip pickupSound;
    public AudioSource playerSource;
    public bool BoostSoundPlayed = false;

    //checkpoint
    private GameMaster gm;
    private void Start()
    {
        
        GameObject[] pickups = GameObject.FindGameObjectsWithTag("Pickup");
        numberOfPickups = pickups.Length;

        rb = GetComponent<Rigidbody>(); 
        count = 0;
        SetScoreText();
        SetPickupText();
        maxBoostMeter = boostMeter;
        isBoosting = false;
        isBraking = false;
        fullStopTimer = fullStop;

        hasBoosted = false;
        boostCooldownTimer = boostCooldown;

        playerSource = GetComponent<AudioSource>();

        gm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>();
    }
    private void Update()
    {
        //jump
        
        downRay = new Ray(transform.position,Vector3.down); //a ray is drawn from the players vector3 down
        Debug.DrawRay(transform.position, Vector3.down, Color.green); //the ray is shown
        if (Physics.Raycast(downRay, out downHit, 1)) //if the downray goes down and hits...
        {
            if (downHit.collider.tag == ("Ground")) //...and hits something with the tag "Ground"...
            {
                isGrounded = true; //...then the character is grounded.
            }
            else
            {
                isGrounded = false; //otherwise it is not
            }
        }
        else
        {
            isGrounded = false; //the character is also not grounded if the ray hits nothing
        }
        if (isGrounded == true && Input.GetButtonDown("Fire1") && PauseMenu.gameIsPaused == false) //if player is grounded and presses the jump button and the game is not paused
        {
            rb.AddForce(new Vector3(0, jumpForce, 0)); //force is added in the upwards direction
            isJumping = true; //the player is jumping
            jumpTimeCounter = jumpTime; //the jumptime counter is set to the same value as jump time
            //SoundManager.playSound(SoundManager.sound.Jump); //plays the jump sound
        }
        if (Input.GetButton("Fire1") && isJumping == true) //if the jump button is held and the player is jumping
        {
            if (jumpTimeCounter > 0.0f) 
            {
                rb.AddForce(new Vector3(0, highJumpForce, 0)); //the higher jump force is added to the players up direction
                jumpTimeCounter -= Time.deltaTime; //the jumphigh counter begins counting down
            }
            else
            {
                isJumping = false;
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            isJumping = false;
        }
        //jumping gravity
        if (isGrounded == false)
        {
            rb.AddForce(new Vector3(0, -gravity * Time.deltaTime, 0)); //if the character is not grounded gravity pulls it down
        }
        //Debug.Log("Velocity: " + rb.velocity.magnitude);
        
        //bounce
        downRay = new Ray(transform.position, Vector3.down); //a ray is drawn from the players vector3 down
        Debug.DrawRay(transform.position, Vector3.down, Color.green); //the ray is shown
        if (Physics.Raycast(downRay, out downHit, 1)) //if the downray goes down and hits...
        {
            float rbSpeed = rb.velocity.magnitude * 2;
            if (downHit.collider.tag == ("Bouncepad")) //...and hits something with the tag "Ground"...
            {
                rb.AddForce(0, bounce * rbSpeed, 0);
            }
        }
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = -Input.GetAxisRaw("Vertical");

        //Debug.Log("MoveHorizontal: " + moveHorizontal);
        //Camera rotation movement

        if (moveVertical > 0.7f && isBraking == false && PauseMenu.gameIsPaused == false) //if the stick is moved forward and player is not braking and the  game is not paused
        {
            float percentOfForwardSpeed = Vector3.Dot(rb.velocity.normalized, camObject.transform.forward); //how fast the player moves forward
            if (rb.velocity.magnitude * percentOfForwardSpeed >= accelLimit) //if the players velocity forward goes above the acceleration limit...
            {
                rb.AddForce(camObject.transform.forward * slowAccel); //the slower force is added to the player in the cameras forward direction...
            }
            else
            {
                rb.AddForce(camObject.transform.forward * normalAccel); //otherwise the normal force is added
            }
        }
        if (-moveVertical > 0.7f && isBraking == false) //if the stick is moved backward and player is not braking
        {
            float percentOfBackwardSpeed = Vector3.Dot(rb.velocity.normalized, -camObject.transform.forward); //how fast the player moves backward
            if (rb.velocity.magnitude * percentOfBackwardSpeed >= accelLimit) //if the players velocity backwards goes above the acceleration limit...
            {
                rb.AddForce(-camObject.transform.forward * slowAccel); //the slower force is added to the player in the cameras backward direction...
            }
            else
            {
                rb.AddForce(-camObject.transform.forward * normalAccel); //otherwise the normal force is added
            }
        }
        if (moveHorizontal > 0.7f && isBraking == false) //if the stick is moved right and player is not braking
        {
            float percentOfRightSpeed = Vector3.Dot(rb.velocity.normalized, camObject.transform.right); //how fast the player moves right
            if (rb.velocity.magnitude * percentOfRightSpeed >= accelLimit) //if the players velocity right goes above the acceleration limit...
            {
                rb.AddForce(camObject.transform.right * slowAccel);
            }
            else
            {
                rb.AddForce(camObject.transform.right * normalAccel);
            }
        }
        if (-moveHorizontal > 0.7f && isBraking == false) //if the stick is moved left and player is not braking
        {
            float percentOfLeftSpeed = Vector3.Dot(rb.velocity.normalized, -camObject.transform.right); //how fast the player moves left
            if (rb.velocity.magnitude * percentOfLeftSpeed >= accelLimit) //if the players velocity left goes above the acceleration limit...
            {
                rb.AddForce(-camObject.transform.right * slowAccel);
            }
            else
            {
                rb.AddForce(-camObject.transform.right * normalAccel);
            }
        }
        //boost
        float boost = Input.GetAxis("Boost"); //the boost button is the right trigger
        if (boostMeter <= 0)
        {
            hasBoosted = true;
        }
        else if (boostMeter >= maxBoostMeter && boost <= 0)
        {
            hasBoosted = false;
        }
        if (boost > 0.2f && boostMeter > 0 && isGrounded == true && isBraking == false && hasBoosted == false && boostCooldown == boostCooldownTimer && PauseMenu.gameIsPaused == false) //if the boost button is pushed and the boost meter is over 0 and the player is grounded and not braking and the boost cooldown is maxed and the game is not paused
        {
            //Debug.Log("Boosting");
            isBoosting = true; //...the player is currently boosting
        }
        else if (boostMeter <= 0)
        {
            isBoosting = false;
        }
        if (isBoosting == true)
        {
            if (moveVertical > 0.1f && isGrounded == true)
            {
                rb.AddForce(camObject.transform.forward * boostAccel); //moves forward with the boost
            }
            else if (moveVertical > 0.1f && isGrounded == false)
            {
                //Debug.Log("Slow down jump forward");
                rb.AddForce(camObject.transform.forward * slowAccel);
            }
            if (-moveVertical > 0.1f && isGrounded == true)
            {
                rb.AddForce(-camObject.transform.forward * boostAccel); //moves backward with the boost
            }
            else if (-moveVertical > 0.1f && isGrounded == false)
            {
                //Debug.Log("Slow down jump back");
                rb.AddForce(-camObject.transform.forward * slowAccel);
            }
            if (moveHorizontal > 0.1f && isGrounded == true)
            {
                rb.AddForce(camObject.transform.right * boostAccel); //moves right with the boost
            }
            else if (moveHorizontal > 0.1f && isGrounded == false)
            {
                //Debug.Log("Slow down jump right");
                rb.AddForce(camObject.transform.right * slowAccel);
            }
            if (-moveHorizontal > 0.1f && isGrounded == true)
            {
                rb.AddForce(-camObject.transform.right * boostAccel); //moves left with the boost
            }
            else if (-moveHorizontal > 0.1f && isGrounded == false)
            {
                //Debug.Log("Slow down jump left");
                rb.AddForce(-camObject.transform.forward * slowAccel);
            }
            boostMeter -= Time.deltaTime; //the boost meter depletes during boosting
            boostCooldown = 0;
            updateBoostBar();
            if (BoostSoundPlayed == false)
            {
                playerSource.PlayOneShot(boostSound);
                BoostSoundPlayed = true;
            }
            //SoundManager.PlaySound(SoundManager.Sound.g3_Boost, transform.position);
        }
        else
         {
            boostCooldown += Time.deltaTime;
             boostMeter += Time.deltaTime * 1/maxBoostMeter; //and the boostmeter starts refilling
             updateBoostBar();
            BoostSoundPlayed = false;
         } 
        if (boostMeter >= maxBoostMeter) //if the boost meter goes over the max...
        {
            boostMeter = maxBoostMeter; //...the boost meter caps at max.
        }
        if (boostMeter <= 0) //if the boost meter goes below 0...
        {
            boostMeter = 0; //...the boost meter remains at 0.
        }
        if (boostCooldown <= 0)
        {
            boostCooldown += Time.deltaTime;
        }
        if (boostCooldown >= boostCooldownTimer)
        {
            boostCooldown = boostCooldownTimer;
        }
        //brake
        float brake = Input.GetAxis("Brake"); //the brake button is the left trigger
        if (brake > 0.2f && isGrounded == true && PauseMenu.gameIsPaused == false) //if the brake button is pushed and the player is grounded and the game is not paused
        {
            isBraking = true; //the player is currently braking
            Brake(); //the brake function is executed
        }
        else
        {
            isBraking = false; // otherwise the player is not currently braking
            Brake(); //the brake function is executed
        }
    }
    //pickup
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup")) //if the player collides with a pickup item...
        {
            other.gameObject.SetActive(false); //...the item becomes inactive...
            count = count + 1; //...and the "count" is increased by one...
            score = score + 200; //...as well as the overall score.
            SetScoreText();
            SetPickupText();
            playerSource.PlayOneShot(pickupSound);
            //SoundManager.PlaySound(SoundManager.Sound.g3_Pickup, transform.position);
        }
        if (other.gameObject.CompareTag("Hazard")) //if the player comes into contact with a hazard
        {
            gameObject.transform.position = gm.lastCheckPointPos; //they return to the latest checkpoint
        }
    }
    //enemy push
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy") //if the player collides with an enemy
        {
            //Debug.Log("Colliding");
            Vector3 pushDirection = collision.contacts[0].point - transform.position; //calculate angle point between collision and player
            pushDirection = -pushDirection.normalized; //get the opposite direction
            rb.AddForce(pushDirection * push); //add force to the player to shove him
        }
    }
    private void updateBoostBar()
    {
        float ratio = boostMeter / maxBoostMeter; //the boost bars number
        //Debug.Log(ratio * 100);
        currentBoostBar.rectTransform.localScale = new Vector3(ratio, 1, 1); //the boost bar scales to the left
        CooldownText.text = boostCooldown.ToString("0");
        //ratioText.text = ((int)(ratio * 100)).ToString("0") + '%'; 
    }
    void SetScoreText()
    {
        Text currentScoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        scoreText = currentScoreText;
        scoreText.text = "Score: " + score.ToString(); //increases the score counter ingame
    }
    void SetPickupText() //shows how many of the total pickups the player has
    {
        Text pickupCount = GameObject.Find("PickupAmount").GetComponent<Text>();
        countText = pickupCount;
        countText.text = "Pickups: " + count.ToString() + "/ " + numberOfPickups.ToString();
    }
    void Brake()
    {
        if (isBraking == true && PauseMenu.gameIsPaused == false)
        {
            rb.velocity -= rb.velocity * Time.deltaTime; //the players velocity slows down
            fullStopTimer -= Time.deltaTime;
        }
        else
        {
            fullStopTimer = fullStop;
        }
        if (fullStopTimer <= 0.1f)
        {
            rb.velocity = new Vector3(0, 0, 0);
        }
        else if (fullStopTimer > 0.1f)
        {
            rb.isKinematic = false;
            rb.WakeUp();
        }
    }
}

