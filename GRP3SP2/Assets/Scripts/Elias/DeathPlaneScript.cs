﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathPlaneScript : MonoBehaviour
{
    public Transform spawnPoint;

    private void OnTriggerEnter(Collider player)
    {
        player.gameObject.transform.position = spawnPoint.position;
        player.gameObject.transform.rotation = spawnPoint.rotation;
    }
}
