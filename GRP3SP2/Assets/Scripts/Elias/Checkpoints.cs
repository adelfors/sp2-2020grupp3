﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoints : MonoBehaviour
{
    private GameController gc;
    //public transform checkpointSpawnPos;
    private void Start()
    {
        gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Checkpoint lagd");
            gc.lastCheckpointPos = transform.position;
            //gc.lastCheckpointPos = checkpointSpawnPos;
            Destroy(gameObject);
        }
    }
}