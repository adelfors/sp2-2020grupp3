﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    //script som ska hålla koll på saker inom spelet som en enhet. sköter alltså globala värden och vissa transforms
    //det här scriptet förstörs EJ när scen byts.
    private GameObject player;
    private static GameController instance;
    [HideInInspector]public Vector3 lastCheckpointPos;
    Vector3 spawnPoint;
    private void Awake()
    {
        if (instance = null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }

        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Die();
        }
    }

    void Die()
    {
        //göra något med score kanske?
        //ska denna kunna hantera både spelarens död och fienders död?
        if (Time.timeScale <= 0)
        {
            Time.timeScale = 1;
        }
        else
        {
            Time.timeScale = 0;
        }

    }

    /*float calculateEndScore(float time, float hp)
    {
        float endScore = 300 - time + score + 100 * hp;
        if (endScore > HiScore)
        {
            HiScore = endScore.;
        }
        return endScore;
    }*/
}
