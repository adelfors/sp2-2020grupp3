﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathPlaneDestroyer : MonoBehaviour
{
    public GameObject DeathPlaneToBeDestroyed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Destroy(DeathPlaneToBeDestroyed);
        }
    }
}
